#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qbluetoothaddress.h>
#include <qbluetoothdevicediscoveryagent.h>
#include <qbluetoothlocaldevice.h>
#include <QMenu>
#include <QDebug>
#include <QObject>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), localDevice(new QBluetoothLocalDevice),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    btChanger = true;
    QObject::connect(ui->button1,&QPushButton::clicked,this,&MainWindow::onButton1Clicked);
    QObject::connect(ui->button2,&QPushButton::clicked,this,&MainWindow::onButton2Clicked);
    QObject::connect(ui->button3,&QPushButton::clicked,this,&MainWindow::onButton3Clicked);

    discoveryAgent = new QBluetoothDeviceDiscoveryAgent();

    // connect(ui->inquiryType, SIGNAL(toggled(bool)), this, SLOT(setGeneralUnlimited(bool)));
    connect(ui->btButton, SIGNAL(clicked()), this, SLOT(startScan()));

    connect(discoveryAgent, SIGNAL(deviceDiscovered(QBluetoothDeviceInfo)),
            this, SLOT(addDevice(QBluetoothDeviceInfo)));
    connect(discoveryAgent, SIGNAL(finished()), this, SLOT(scanFinished()));


    //connect(ui->list, SIGNAL(itemActivated(QListWidgetItem*)),
           // this, SLOT(itemActivated(QListWidgetItem*)));

    //connect(localDevice, SIGNAL(hostModeStateChanged(QBluetoothLocalDevice::HostMode)),
            //this, SLOT(hostModeStateChanged(QBluetoothLocalDevice::HostMode)));
    localDevice->powerOn();
    // hostModeStateChanged(localDevice->hostMode());
    //hostModeStateChanged(localDevice->hostMode());
    // add context menu for devices to be able to pair device
    //ui->list->setContextMenuPolicy(Qt::CustomContextMenu);
    //connect(ui->list, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(displayPairingMenu(QPoint)));
    //connect(localDevice, SIGNAL(pairingFinished(QBluetoothAddress,QBluetoothLocalDevice::Pairing))
        //, this, SLOT(pairingDone(QBluetoothAddress,QBluetoothLocalDevice::Pairing)));




}
void MainWindow::onButton1Clicked(){
    ui->bt_Status->setText("bUTTON 1 pressed");
    ui->text1->setStyleSheet("background-color: orange;");
}
void MainWindow::onButton2Clicked(){
    ui->bt_Status->setText("bUTTON 2 pressed");
    ui->text1->setText("unter2");
    ui->text1->setStyleSheet("background-color: red;");
}
void MainWindow::onButton3Clicked(){
    ui->bt_Status->setText("bUTTON 3 pressed");
    ui->text1->setText("unter3");
    ui->text1->setStyleSheet("background-color: green;");
}
void MainWindow::addDevice(const QBluetoothDeviceInfo &info)
{
    ui->btMessages->append(QString("Found: %1 - %2").arg(info.address().toString()).arg(info.name()));
    ui->btDevices->addItem(QString("%1 - %2").arg(info.address().toString()).arg(info.name()));
}
void MainWindow::startScan(){
    if(btChanger){
        discoveryAgent->start();
        ui->btMessages->append("<hr />Scan Start</hr >");
        btChanger=false;
        ui->btButton->setStyleSheet("background-color: green;");
    }
    else {
        discoveryAgent->stop();
        ui->btMessages->append("<hr />Scan stop</hr >");
        btChanger=true;
        ui->btButton->setStyleSheet("background-color: red;");
    }

}

void MainWindow::scanFinished()
{

   ui->btMessages->append("<hr />Scan Finished</hr >");
}

MainWindow::~MainWindow()
{
    delete ui;
    delete localDevice;
}


